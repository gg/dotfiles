# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi
if [ -f ~/.bashrc.local ]; then
	. ~/.bashrc.local
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

export PATH="$HOME/.cargo/bin:$PATH"
