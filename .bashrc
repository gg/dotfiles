# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

# remove annoying global aliases
noalias() {
  x=`type $1 2>&1 | grep " is aliased to "` && unalias $1
}
# unalias l. ll ls vi
noalias l.
noalias ll
noalias ls
noalias vi
noalias egrep
noalias fgrep
noalias grep

alias ..="tcsh -l"
alias sux="sudo tcsh -l"

set -P
