
set prompt='%m:%n> '

setenv PERL5LIB "$HOME/perl:/usr/local/lib/perl5"
setenv SHELL $shell
set path=($HOME/bin $path)

alias sux sudo tcsh -l
alias S SLS -P
alias tsv ~/bin/csv --UTF8 --TAB
setenv EDITOR `which vi`

setenv LC_ALL en_US.UTF-8
setenv LESSCHARSET utf-8

setenv PYTHONSTARTUP ~/.pythonrc

alias cal ncal -wbM

if (-x ~/.login.local) then
  source ~/.login.local
endif

if (-x ~/.cargo/cenv) then
  source ~/.cargo/cenv
endif
