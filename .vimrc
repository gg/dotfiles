
" turn off rainbow effect [ http://www.howtogeek.com/howto/linux/disable-syntax-highlighting-in-vim/ ]
syntax off
set ai
set copyindent
set ts=2
set number
