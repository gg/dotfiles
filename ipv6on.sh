#!/bin/sh
sysctl -a net.ipv6.conf 2>/dev/null | awk '/disable_ipv6/ { print $1"=0"}' | xargs sysctl
