#!/usr/bin/perl

=head1 NAME

  show_fingerprints.pl

=head1 DESCRIPTION

  display fingerprints of all public keys and keys in authorized_keys

=cut

use strict;

my ($version, $has_E)= get_ssh_version();
my $hash_algorithm= 'MD5';

my @files= <*.pub>;

while (my $arg= shift (@ARGV))
{
  if ($arg eq '--') {}
  elsif ($arg =~ m#^--(.+)#)
  {
  }
  elsif ($arg =~ m#^-(.+)#)
  {
    # print ">>> arg=[$arg] 1=[$1]\n";
    my @opts= split ('', $1);
    foreach my $opt (@opts)
    {
      if ($opt eq 'E')
      {
        $has_E= 1;
        $hash_algorithm= shift (@ARGV);
        # print "NOTE: hash_algorithm=[$hash_algorithm]\n";
      }
    }
  }
  else
  {
    push (@files, $arg);
  }
}

# print "files: ", join (' ', @files), "\n";
push (@files, <authorized_keys*>);

printf ("%-24s %5s %-51s %-7s %s\n", qw(file size fingerprint type notes));
foreach my $file (@files)
{
  my @cmd= qw(ssh-keygen -l);
  push (@cmd, '-E', $hash_algorithm) if ($has_E);
  push (@cmd, '-f', $file);
  # print "cmd: [", join (' ', @cmd), "\n";

  my $res= `@cmd`;
  my @lines= split ("\n", $res);
  foreach my $line (@lines)
  {
    my ($size, $fp, $notes)= split (' ', $line, 3);

    my $type= 'unknown';
    if ($notes =~ m#(.+)\s+\((.+)\)#) { ($notes, $type)= ($1, $2) }
    $fp= join(':', $hash_algorithm, $fp) unless ($has_E);

    printf ("%-24s %5d %-51s %-7s %s\n", $file, $size, $fp, $type, $notes);
  }
}

sub get_ssh_version
{
  # my @cmd= qw(ssh-keygen --version); # apparently, there is way to display the version directly
  my @cmd= qw(ssh-keygen -?); # apparently, --version does not fail as expected on some systems, e.g. openssh-5.3p1-118.1.el6_8.x86_64
  # print "cmd: ", join (' ', @cmd), "\n";
  my $res= `@cmd 2>&1`;
  # print "res=[$res]\n";

  my $version= 'unknown';
  my $has_E= 0;
  foreach my $l (split ("\n", $res))
  {
    # print "[$l]\n";

       if ($l =~ '  -L          Print the contents of a certificate.') { $has_E= 0; }
    elsif ($l eq '       ssh-keygen -l [-v] [-E fingerprint_hash] [-f input_keyfile]') { $has_E= 1; }
  }

  if ($version eq 'unknown')
  {
    my @cmd2= qw(ssh -V); # let's try this ...
    my $res2= `@cmd2 2>&1`;
    print "res2=[$res2]\n";
    # TODO: figure out how to match that ...
  }

  ($version, $has_E);
}

=head1 COMPATIBILITY

=head2 Ubuntu

* openssh-client 1:7.2p2-4ubuntu2.1

=head2 CentOS

* openssh-5.3p1-118.1.el6_8.x86_64

=head1 TODO

optionally display fingerprints using other hashing algorithms

=cut
